## Integração com Firebase

https://firebase.google.com/docs/hosting/deploying?hl=pt-br

Instalar a CLI do Firebase
```javascript
npm install -g firebase-tools
````

Inicializar o site
```javascript
$ firebase init
````

Implantar seu site
```javascript
$ firebase deploy
```

## Desenvolvimento Local

https://github.com/remy/nodemon
```javascript
npm install -g nodemon
```