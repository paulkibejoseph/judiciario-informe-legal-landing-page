const express = require("express");
const app = express();

const HOSTNAME = "localhost";
const PORT = process.env.PORT || 8080;

app.use(express.static('public'));

const server = app.listen(PORT, () => {
  console.log(`Server running at http://${HOSTNAME}:${PORT}/`);
});

module.exports = app;
